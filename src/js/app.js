(function mainNav() {

    $('.header .menu-toggle').on('click', function () {
        toggleNav();
    })
    $('.main-nav .close-toggle').on('click', function () {
        toggleNav();
    })
    $(document).on('click', '.menu-overlay', function () {
        toggleNav();
    })
    $('.main-nav').swipeLeft(function () {
        toggleNav();
    })

    function toggleNav() {
        $body = $('body');
        $overlay = $('<div class="menu-overlay"></div>');
        $header = $('.header');
        if (!$body.hasClass('menu-active')) {
            $header.append($overlay);
            setTimeout(function () {
                $body.addClass('menu-active');
            }, 50);
        } else {
            $body.removeClass('menu-active');
            setTimeout(function () {
                $('.menu-overlay').remove();
            }, 250);
        }
    }
})();