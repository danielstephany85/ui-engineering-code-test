(function contentToggle($){
    $('.content-toggle').on('click', function(){
        var $this = $(this);
        $this.prev('.toggle-container').slideToggle(300);
        if(!$this.hasClass('active')){
            $this.text('Close');
        } else {
            $this.text('View All');
        }
        $this.toggleClass('active');
    });
})(jQuery);