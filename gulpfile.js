'use strict';

const gulp =    require('gulp');
const concat =  require('gulp-concat'); //used for concatinating js files
const uglify =  require('gulp-uglify'); //used to minify js files
const rename =  require('gulp-rename'); //used to rename files
const sass =    require('gulp-sass');	//used to compile sass
const maps =    require('gulp-sourcemaps'); //used to make sourcemaps files for js, css, scss,less
const cssnano = require('gulp-cssnano'); // used to minify css
const del =     require('del'); // used to delete files for clean up
const autoprefixer = require('gulp-autoprefixer'); //used to auto add vendor prefixes to css
const browserSync =  require('browser-sync'); //reloads browser after saving a change to a file
const prettify =     require('gulp-prettify'); //properly indents html files
const plumber = 	   require('gulp-plumber'); //error handler for gulp

var supported = [
	'last 5 versions',
	'safari >= 8',
	'ie >= 9',
	'ff >= 20',
	'ios 6',
	'android 4'
];

// js files to be concatinated in this order
var vendorScripts = [
	'node_modules/jquery/dist/jquery.min.js'
		];
var mainScripts = [
	'./src/js/swipe-events.js',
	'./src/js/app.js',
	'./src/js/content-toggle.js'
];


// prettifies html and places it in dist
gulp.task('get_html', function() {
	return gulp.src('src/**/*.html')
	.pipe(prettify())
	.pipe(gulp.dest('./dist')) // output the rendered HTML files to the "dist" directory
	.pipe(browserSync.stream());
});

// concats and minifies vendor scripts.
gulp.task('get_scripts', function() {
	return gulp.src(vendorScripts)
	.pipe(concat('vendors.js'))
	.pipe(uglify())
	.pipe(rename("vendors.min.js"))
	.pipe(gulp.dest('./dist/js'))
	.pipe(browserSync.stream());
});


// concatinates js from the scripts var into one file app.js,
// minifys app.js into app.min.js,
// then writes the source maps,
// places both files in ./js,
// and reloads the browser
gulp.task('minifyScripts', function() {
	return gulp.src(mainScripts)
	.pipe(plumber())
	.pipe(maps.init())
	.pipe(concat('app.js'))
	.pipe(uglify())
	.pipe(rename("app.min.js"))
	.pipe(maps.write('./'))
	.pipe(gulp.dest('./dist/js'))
	.pipe(browserSync.stream());
});
//does same as above minus maps
gulp.task('minifyScripts-noMaps', function () {
	return gulp.src(mainScripts)
		.pipe(plumber())
		.pipe(concat('app.js'))
		.pipe(uglify())
		.pipe(rename("app.min.js"))
		.pipe(gulp.dest('./dist/js'))
		.pipe(browserSync.stream());
});

// compiles sass from scss/main.scss to main.css
// adds prefixes with auto prefixer to css
// minifies css to main.min.css
// writes source maps
// places both in dist/css
gulp.task('minifyCss', function() {
	return gulp.src('src/scss/main.scss')
	.pipe(plumber())
	.pipe(maps.init())
	.pipe(sass())
	.pipe(cssnano({
		autoprefixer: { browsers: supported, add: true }
	}))
	.pipe(rename("main.min.css"))
	.pipe(maps.write('./'))
	.pipe(gulp.dest('./dist/css'))
	.pipe(browserSync.stream());
});
//does same as above minus maps
gulp.task('minifyCss-noMaps', function () {
	return gulp.src('src/scss/main.scss')
		.pipe(plumber())
		.pipe(sass())
		.pipe(cssnano({
			autoprefixer: { browsers: supported, add: true }
		}))
		.pipe(rename("main.min.css"))
		.pipe(gulp.dest('./dist/css'))
		.pipe(browserSync.stream());
});

// starts development server at localhost:3000
// watches html, js, and scss and runs associated tasks
gulp.task('watchFiles',['build'], function() {
	browserSync.init({
		server: "./dist",
		notify: false
    });
	gulp.watch('./src/scss/**/*.scss', ['minifyCss']);
	gulp.watch('./src/js/**/*.js', ['minifyScripts']);
	gulp.watch('./src/**/*.html', ['get_html']);
});

// builds the dist directory and by running associated tasks
// that place their contents in dist, and by placing the folders and
// files returned from gulp.src into dist
gulp.task('build', ['get_html', 'get_scripts', 'minifyScripts', 'minifyCss'], function() {
	return gulp.src(["src/images/**"], { base: './src'})
	.pipe(gulp.dest('dist'));
});


//production build provides only minified files.
gulp.task('prod', ['get_html', 'get_scripts', 'minifyScripts-noMaps', 'minifyCss-noMaps'], function () {
	return gulp.src(["src/images/**"], { base: './src' })
		.pipe(gulp.dest('dist'));
});


// deletes all folders created by gulp tasks
gulp.task('clean', function(){
	del(['dist']);
});

// default task is set to start the watch listeners
gulp.task('default', [], function() {
	gulp.start(['watchFiles']);
});
