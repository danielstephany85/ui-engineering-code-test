# UI Engineering code test #

IF you clone this repo the UI Engineering code test comes pre built for production, You can just place the contents of the dist folder into a basic apache server
and everything should run.

### If you wish to run the build follow the steps below ###

* first you will need to download the dependencies using npm install or yarn.
* To build the project for production navigate to the root of the project in the command line and run: gulp prod
* To build the project including all sourcemaps navigate to the root of the project in the command line and run: gulp build
* To build the project and start the dev server on port 3000 navigate to the root of the project in the command line and run: gulp

